package main

import (
	"fmt"
	"log"
)

const (
	// RED Color
	RED = "\033[1;31m"
	// GREEN Color
	GREEN = "\033[1;32m"
	// BLUE Color
	BLUE = "\033[1;34m"
	// GRAY Color
	GRAY = "\033[1;37m"
	// NOCOLOR Color
	NOCOLOR = "\033[0m"
)

func logWithColor(color string, msg string) {
	log.Printf("%s%s%s\n", color, msg, NOCOLOR)
}

// Red outputs the string in red
func Red(msg string, vals ...interface{}) {
	m := fmt.Sprintf(msg, vals...)
	logWithColor(RED, m)
}

// Blue outputs the string in blue
func Blue(msg string, vals ...interface{}) {
	m := fmt.Sprintf(msg, vals...)
	logWithColor(BLUE, m)
}

// Green outputs the string in green
func Green(msg string, vals ...interface{}) {
	m := fmt.Sprintf(msg, vals...)
	logWithColor(GREEN, m)
}

// Gray outputs the string in gray
func Gray(msg string, vals ...interface{}) {
	m := fmt.Sprintf(msg, vals...)
	logWithColor(GRAY, m)
}
