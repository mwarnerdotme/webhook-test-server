package main

import (
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()

	app.Post("", handleWebhook)

	port := "9999"
	log.Printf("Starting API server on port %s...", port)
	if err := app.Listen(":" + port); err != nil {
		log.Fatalf("Could not start web server: %+v\n", err)
	}
}

func handleWebhook(c *fiber.Ctx) (err error) {
	ip := c.IP()
	body := string(c.Body())

	Green("New webhook from %s", ip)
	fmt.Println(body)

	return c.SendStatus(fiber.StatusOK)
}
